import 'package:flutter/material.dart';

//*************************************** CLASS CONST GLOBAL ************************************** */
class ConstGlobal{

  static final nameDrawer = [
    "MAIN",
    "CALENDAR",
    "SETTING"
  ]; 

  static final List<IconData> iconDrawer = [
    Icons.home,
    Icons.calendar_today,
    Icons.settings
  ]; 

}
//************************************************************************************************* */