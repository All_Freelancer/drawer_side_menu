import 'package:drawer_side_menu/src/ui/widgets/drawer.dart';
import 'package:flutter/material.dart';
import 'package:kf_drawer/kf_drawer.dart';

//**************************************** CLASS SETTINGS PAGE **************************************  */
class SettingsPage extends KFDrawerContent {
  //Variable  
  final String title; 

  //******************************** CONSTRUCT ************************************** */
  SettingsPage(this.title); 

  //******************************** CALL STATE SETTINGS PAGE ************************ */
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

//*************************************** STATE SETTINGS PAGE *************************************** */
class _SettingsPageState extends State<SettingsPage> {

  //*********************************** WIDGET ROOT ******************************* */
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Center(
        child: Column(
          children: <Widget>[

            Row(
              children: <Widget>[
                DrawerWidget.buildOpen(widget.onMenuPressed), //CLOSE AND OPEN DRAWER 
              ],
            ),
          
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(widget.title),
                ],
              ),
            ),
          ],

        ),
      ),
    );
  }
}
//*********************************************************************************************** */